<?php 

class Instrumento extends Eloquent {
	protected $table = 'instrumentos';
	protected $softDelete = true;

	protected $fillable = array('nome', 'descricao', 'tonalidade' );

	public function musico() {
		return $this->hasMany('Musico', 'instrumento_id');
	}

	public function partituras() {
		return $this->belongsToMany('Partitura', 'instrumentos_partituras');
	}
}