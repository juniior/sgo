<?php 

class Partitura extends Eloquent {
	protected $table = 'partituras';
	protected $softDelete = true;
	
	protected $fillable = array('nome', 'autor', 'observacao', 'arquivo', 'user_id' );
	
	public function instrumentos(){
		return $this->belongsToMany('Instrumento','instrumentos_partituras');
	}

	public function user(){
		return $this->belongsTo('User');
	}
}