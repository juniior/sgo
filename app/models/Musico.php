<?php 

class Musico extends Eloquent {
	protected $table = 'musicos';
	protected $softDelete = true;

	protected $fillable = array('nome', 'dt_nascimento', 'telefone', 'celular', 'imagem', 'user_id', 'instrumento_id', 'cep', 'numero');

	public function instrumento(){
		return $this->belongsTo('Instrumento');
	}

	public function user(){
		return $this->belongsTo('User');
	}
}