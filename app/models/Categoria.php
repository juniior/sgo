<?php 

class Categoria extends Eloquent {
	protected $table = 'categorias';
	protected $softDelete = true;

	protected $fillable = array('nome', 'descricao', 'categoria_id' );

	public function filhas() {
	    return $this->hasMany('Categoria', 'categoria_id'); 
	}

	public function pai(){
	    if($this->categoria_id !== null && $this->categoria_id > 0){
	        return $this->belongsTo('Categoria', 'categoria_id');
	    } else {
	        return null;
	    }
	}
}