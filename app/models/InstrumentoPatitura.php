<?php 

class InstrumentoPartitura extends Eloquent {
	protected $table = 'instrumentos_partituras';
	public $timestamps = false;

	protected $fillable = array('instrumento_id', 'partitura_id');
	
	public function instrumento() {
		return $this->belongsTo('Instrumento');
	}

	public function partitura() {
		return $this->belongsTo('Partitura');
	}
}