@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-11">
			<h2><i class="glyphicon glyphicon-headphones"></i> Instrumentos</h2>
		</div>
		<div class="col-md-1">
			{{ link_to('instrumentos/create', 'Novo', array ('class' => 'btn btn-primary')) }}
		</div>
	</div>
	<div class="navbar-form navbar-right">
		{{ Form::open ( array ('url' => 'instrumentos', 'method' => 'get', 'class'=>'form-group ', 'role' => 'form'))  }}
			{{ Form::text('nome', $nome, array('placeholder' => 'Nome', 'class'=>'form-control')) }}
			{{ Form::button('Pesquisar', array('type' => 'submit', 'class' => 'btn  btn-success')) }}
		{{ Form::close() }}
		<hr>
	</div>
	<div class="row">
		@if($instrumentos->getItems())
			<table class="table table-bordered table-condensed table-striped">
				<thead>
					<tr>
						<th><a href="{{ URL::to('intrumento?sort=nome'. $str) }}">Nome</a></th>
						<th class="col-sm-1"></th>
					</tr>
				</thead>
				<tbody>
				@foreach ($instrumentos as $instrumento)
					<tr>
						<td><span class="label label-info">{{ e($instrumento->tonalidade) }}</span> {{ e($instrumento->nome) }}</td>
						<td>
							<div class="btn-group btn-group-xs">
{{ link_to('instrumentos/'.$instrumento->id.'/edit', '', array('class' => 'btn btn-primary glyphicon glyphicon-edit', 'title'=>'Editar registro')) }}
{{ link_to('instrumentos/'.$instrumento->id.'/apagar', '', array('class' => 'btn btn-danger glyphicon glyphicon-remove', 'title'=>'Apagar registro')) }}
							</div>
						</td>
						
					</tr>
				@endforeach
				</tbody>
			</table>
			<div id="data_paginate">
				{{ $pagination }}
				<p>Exibindo de {{ $instrumentos->getFrom() }} até {{ $instrumentos->getTo() }} de {{ $instrumentos->getTotal() }} registros.</p>
			</div>
		@else 
			<p class="label label-warning">{{ Util::message('MSG008') }}</p>
		@endif
	</div>
@stop