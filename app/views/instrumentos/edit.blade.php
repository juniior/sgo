@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-11">
			<h3>Alterar instrumento</h3>
		</div>
		<div class="col-md-1">
			{{ link_to('instrumentos', 'Voltar', array ('class' => 'btn btn-primary')) }}
		</div>
		<hr>
	</div>
	
	{{ Form::open ( array ('url' => 'instrumentos/'.$instrumento->id, 'method'=>'put', 'class'=>'well'))  }}
		<div class="row">
			<div class="col-xs-6">
				{{ Form::label('nome', 'Nome', array( 'class' => '')) }} <br>
				{{ Form::text('nome', Input::old('nome', $instrumento->nome), array('placeholder' => 'Nome', 'class'=>'form-control')) }}
			</div>
			<div class="col-xs-3">
				{{ Form::label('tonalidade', 'Tonalidade', array( 'class' => '')) }} <br>
				{{ Form::select('tonalidade', array('B♭'=>'B♭', 'E♭'=>'E♭', 'Fá'=>'Fá', 'Dó'=>'Dó'), $instrumento->tonalidade, array('class'=>'form-control')) }}
			</div>
			<div class="col-xs-12">
				{{ Form::label('descricao', 'Descrição', array( 'class' => '')) }} <br>
				{{ Form::textarea('descricao', Input::old('descricao', $instrumento->descricao), array('placeholder' => 'Descrição', 'class'=>'form-control')) }}
			</div>
		</div>
		<div class="row">
		<br>
			<div class="col-xs-3 pull-right">
				{{ Form::submit('Salvar', array('class'=>'btn btn-primary form-control')) }}	        
		    </div>
		</div>
	{{ Form::close() }}
@stop