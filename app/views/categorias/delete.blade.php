@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-11">
			<h3>Apagar categoria</h3>
		</div>
		<div class="col-md-1">
			{{ link_to('categorias', 'Voltar', array ('class' => 'btn btn-primary')) }}
		</div>
		<hr>
	</div>
	
	{{ Form::model ($categoria, array ('url' => 'categorias/'.$categoria->id, 'method'=>'put', 'class'=>'well')) }}
		<div class="row">
				<div class="col-xs-6">
				{{ Form::label('nome', 'Nome') }} <br>
				{{ Form::text('nome', Input::old('nome'), array('placeholder' => 'Nome', 'class'=>'form-control', 'readonly'=>'readonly')) }}
			</div>
		</div>
		<div class="row">
			<br>
			<div class="col-xs-12">
				{{ Form::label('observacao', 'Observação') }} <br>
				{{ Form::textArea('observacao', Input::old('observacao'), array('placeholder' => 'Observação', 'class'=>'form-control', 'readonly'=>'readonly')) }}
			</div>
		</div>

		<div class="row">
		<br>
			<div class="col-xs-3 pull-right">
				{{ Form::open(array('url' => 'categorias/' . $categoria->id, 'method' => 'delete', 'data-confirm' => 'Deseja realmente excluir o registro selecionado?')) }}
				{{ Form::button('Apagar Registro', array('type' => 'submit', 'class' => 'btn btn-danger form-control', 'title' => 'Apagar')) }}
				{{ Form::close() }}        
		    </div>
		</div>
	{{ Form::close() }}
@stop