@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-11">
			<h2>Categorias</h2>
		</div>
		<div class="col-md-1">
			{{ link_to('categorias/create', 'Novo', array ('class' => 'btn btn-primary')) }}
		</div>
	</div>
	<div class="navbar-form navbar-right">
		{{ Form::open ( array ('url' => 'categorias', 'method' => 'get', 'class'=>'form-group ', 'role' => 'form'))  }}
			{{ Form::text('nome', $nome, array('placeholder' => 'Nome', 'class'=>'form-control')) }}
			{{ Form::button('Pesquisar', array('type' => 'submit', 'class' => 'btn  btn-success')) }}
		{{ Form::close() }}
		<hr>
	</div>
	<div class="row">
		@if($categorias->getItems())
			<table class="table table-bordered table-condensed table-striped">
				<thead>
					<tr>
						<th class="col-sm-2"><a href="{{ URL::to('categoria?sort=nome'. $str) }}">Nome</a></th>
						<th class="">Descrição</th>
						<th class="col-sm-1"></th>
					</tr>
				</thead>
				<tbody>
				@foreach ($categorias as $categoria)
					<tr>
						<td>{{ e($categoria->nome) }} <span class="label label-info">{{$categoria->filhas->count()}}</span></td>
						<td>{{ e($categoria->descricao) }}</td>
						<td>
							<div class="btn-group btn-group-xs">
{{ link_to('categorias/'.$categoria->id, '', array('class' => 'btn btn-success glyphicon glyphicon-th-list', 'title'=>'Ver sub-categorias')) }}
{{ link_to('categorias/'.$categoria->id.'/edit', '', array('class' => 'btn btn-primary glyphicon glyphicon-edit', 'title'=>'Editar registro')) }}
@if ($categoria->filhas->count() < 1)
{{ link_to('categorias/'.$categoria->id.'/apagar', '', array('class' => 'btn btn-danger glyphicon glyphicon-remove', 'title'=>'Apagar registro')) }}
@endif
							</div>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
			<div id="data_paginate">
				{{ $pagination }}
				<p>Exibindo de {{ $categorias->getFrom() }} até {{ $categorias->getTo() }} de {{ $categorias->getTotal() }} registros.</p>
			</div>
		@else 
			<p class="label label-warning">Não foram encontrados registros</p>
		@endif
	</div>
@stop