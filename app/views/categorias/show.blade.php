@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-11">
			<h3>Adicionar categoria</h3>
		</div>
		<div class="col-md-1">
			{{ link_to('categorias', 'Voltar', array ('class' => 'btn btn-primary')) }}
		</div>
		<hr>
	</div>
	
	{{ Form::model ($categoria, array ('url' => 'categorias/'.$categoria->id, 'method'=>'put', 'class'=>'well')) }}
	<div class="row">
		<div class="col-xs-6">
			<div class="row">
				<div class="col-xs-12">
					{{ Form::label('nome', 'Nome') }} <br>
					{{ Form::text('nome', Input::old('nome'), array('placeholder' => 'Nome', 'class'=>'form-control', 'readonly'=>'readonly')) }}
				</div>
			</div>
			<div class="row">
				<br>
				<div class="col-xs-12">
					{{ Form::label('descricao', 'Descrição') }} <br>
					{{ Form::textArea('descricao', Input::old('descricao'), array('placeholder' => 'Descrição', 'class'=>'form-control', 'readonly'=>'readonly')) }}
				</div>
			</div>
			<div class="row">
				<br>
				<div class="col-xs-4 pull-right">
				{{ link_to_action('CategoriasController@edit', 'Editar', array ('id' => $categoria->id), array ('class' => 'btn btn-warning col-xs-12')) }}
			    </div>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="row">
				@if($categoria->filhas->count() > 0)
				<div class="col-xs-12">
					<table class="table table-bordered table-condensed table-striped">
						<thead>
							<tr>
								<th class="col-sm-2">Nome</th>
								<th class="">Descrição</th>
								<th class="col-sm-2"></th>
							</tr>
						</thead>
						<tbody>
						@foreach ($categoria->filhas as $categoriaItem)
							<tr>
								<td>{{ e($categoriaItem->nome) }}</td>
								<td>{{ e($categoriaItem->descricao) }}</td>
								<td>
									<div class="btn-group btn-group-xs">
{{ link_to('categorias/'.$categoriaItem->id.'/edit', '', array('class' => 'btn btn-primary glyphicon glyphicon-edit', 'title'=>'Editar registro')) }}
{{ link_to('categorias/'.$categoriaItem->id.'/apagar', '', array('class' => 'btn btn-danger glyphicon glyphicon-remove', 'title'=>'Apagar registro')) }}
									</div>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>

				</div>
				@else
				<div class="col-xs-12">
				<br>
					<p class="label label-warning">Não existem sub-categorias registradas para essa categoria</p>	
				</div>
				
				@endif
				<div class="col-xs-12">
				<br>
					{{ link_to_action('CategoriasController@create', 'Adicionar Sub-categoria', array ('categoria_id' => $categoria->id), array ('class' => 'btn btn-success col-xs-5 pull-right')) }}
				</div>
			</div>
		</div>
	</div>
	{{ Form::close() }}
@stop