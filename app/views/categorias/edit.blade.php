@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-11">
			<h3>Alterar categoria</h3>
		</div>
		<div class="col-md-1">
			{{ link_to('categorias', 'Voltar', array ('class' => 'btn btn-primary')) }}
		</div>
		<hr>
	</div>
	
	{{ Form::model ($categoria, array ('url' => 'categorias/'.$categoria->id, 'method'=>'put', 'class'=>'well')) }}
	<div class="row">
		<div class="col-xs-12">
			{{ Form::label('nome', 'Nome') }} <br>
			{{ Form::text('nome', Input::old('nome'), array('placeholder' => 'Nome', 'class'=>'form-control')) }}
		</div>
	</div>
	<div class="row">
		<br>
		<div class="col-xs-12">
			{{ Form::label('descricao', 'Descrição') }} <br>
			{{ Form::textArea('descricao', Input::old('descricao'), array('placeholder' => 'Descrição', 'class'=>'form-control')) }}
		</div>
	</div>
	
	<div class="row">
		<br>
		<div class="col-xs-3 pull-right">
			{{ Form::submit('Salvar', array('class'=>'btn btn-primary form-control')) }}	        
	    </div>
	</div>
	{{ Form::close() }}
@stop