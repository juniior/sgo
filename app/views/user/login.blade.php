@extends('layouts.master')

@section('content')
<style>
	body {
		padding-top: 60px;
		padding-bottom: 40px;
		background-color: #eee;
	}

	.form-signin {
		max-width: 330px;
		padding: 15px;
		margin: 0 auto;
	}
	.form-signin .form-signin-heading,
	.form-signin .checkbox {
		margin-bottom: 10px;
	}
	.form-signin .checkbox {
		font-weight: normal;
	}
	.form-signin .form-control {
		position: relative;
		height: auto;
		-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
				box-sizing: border-box;
		padding: 10px;
		font-size: 16px;
	}
	.form-signin .form-control:focus {
		z-index: 2;
	}
	.form-signin input[type="email"] {
		margin-bottom: -1px;
		border-bottom-right-radius: 0;
		border-bottom-left-radius: 0;
	}
	.form-signin input[type="password"] {
		margin-bottom: 10px;
		border-top-left-radius: 0;
		border-top-right-radius: 0;
	}
</style>
	
<form method="POST" class="form-signin" action="{{{ Confide::checkAction('UserController@do_login') ?: URL::to('/user/login') }}}" accept-charset="UTF-8">
	<input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
	<fieldset>
		<div class="form-group">
			<label for="email">{{{ Lang::get('confide::confide.username_e_mail') }}}</label>
			<input class="form-control" tabindex="1" placeholder="{{{ Lang::get('confide::confide.username_e_mail') }}}" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
		</div>
		
		<div class="form-group">
			<label for="password">
			{{{ Lang::get('confide::confide.password') }}}
			<small>
			<a href="{{{ (Confide::checkAction('UserController@forgot_password')) ?: 'forgot' }}}">{{{ Lang::get('confide::confide.login.forgot_password') }}}</a>
			</small>
			</label>
			<input class="form-control" tabindex="2" placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="password">
		</div>
		
		<div class="form-group">
			<label for="remember" class="checkbox">{{{ Lang::get('confide::confide.login.remember') }}}
			<input type="hidden" name="remember" value="0">
			<input tabindex="4" type="checkbox" name="remember" id="remember" value="1">
			</label>
		</div>
		
		@if ( Session::get('error') )
		<div class="alert alert-error">{{{ Session::get('error') }}}</div>
		@endif

		@if ( Session::get('notice') )
		<div class="alert alert-info">{{{ Session::get('notice') }}}</div>
		@endif

		<div class="form-group">
			<button tabindex="3" type="submit" class="btn btn-primary btn-block">{{{ Lang::get('confide::confide.login.submit') }}}</button>
		</div>
	</fieldset>
</form>
	
@stop