@extends('layouts.master')

@section('content')

<div class="row">
	<div class="col-md-12">
		<h3>Bem vindo - Criar nova conta</h3>
		<p>Seja bem-vindo ao SGO - Registra-se para ter acesso ao nosso sistema.</p>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
	@if ( Session::get('error') )
        <div class="alert alert-error alert-danger">
            @if ( is_array(Session::get('error')) )
                {{ head(Session::get('error')) }}
            @endif
        </div>
    @endif

    @if ( Session::get('notice') )
        <div class="alert">{{ Session::get('notice') }}</div>
    @endif
	</div>
</div>

{{ Form::open ( array ('url' => 'user', 'class'=>'well'))  }}
	<div class="row">
		<div class="col-xs-4">
			{{ Form::label('nome', 'Nome completo') }} <br>
			{{ Form::text('nome', Input::old('nome'), array('placeholder' => 'Nome completo', 'class'=>'form-control')) }}
		</div>
		<div class="col-xs-2">
			{{ Form::label('dt_nascimento', 'Dt. Nascimento') }} <br>
			{{ Form::text('dt_nascimento', Input::old('dt_nascimento'), array('placeholder' => 'Dt. Nascimento', 'class'=>'form-control data')) }}
		</div>
		<div class="col-xs-2">
			{{ Form::label('sexo', 'Sexo') }} <br>
			{{ Form::select('sexo',array('Feminino'=>'Feminino', 'Masculino'=>'Masculino'), null, array('class'=>'form-control')) }}
		</div>
		<div class="col-xs-4">
			{{ Form::label('instrumento_id', 'Instrumento') }} <br>
			{{ Form::select('instrumento_id', $instrumentos, $selected = NULL, array('class'=>'form-control')) }}
		</div>
	</div>

	<div class="row">
		<br>
		<div class="col-xs-4">
			{{ Form::label('username', 'Nome de usuário') }} <br>
			{{ Form::text('username', Input::old('username'), array('placeholder' => 'Nome de usuário', 'class'=>'form-control')) }}
		</div>
		<div class="col-xs-4">
			{{ Form::label('email', 'E-mail') }} <br>
			{{ Form::text('email', Input::old('email'), array('placeholder' => 'E-mail', 'class'=>'form-control')) }}
		</div>
		<div class="col-xs-2">
			{{ Form::label('password', 'Senha') }} <br>
			{{ Form::password('password', array('placeholder' => 'Senha', 'class'=>'form-control')) }}
		</div>
		<div class="col-xs-2">
			{{ Form::label('password_confirmation', 'Confirmar senha') }} <br>
			{{ Form::password('password_confirmation', array('placeholder' => 'Confirmar senha', 'class'=>'form-control')) }}
		</div>
	</div>
       
	<div class="row">
		<br>
		<div class="col-xs-2">
			{{ Form::label('cep', 'CEP') }} <br>
			{{ Form::text('cep', Input::old('cep'), array('placeholder' => 'CEP', 'class'=>'form-control cep')) }}
		</div>
		<div class="col-xs-1">
			{{ Form::label('numero', 'Numero') }} <br>
			{{ Form::text('numero', Input::old('numero'), array('placeholder' => 'N.', 'class'=>'form-control numero')) }}
		</div>
		<div class="col-xs-2">
			{{ Form::label('celular', 'Celular') }} <br>
			{{ Form::text('celular', Input::old('celular'), array('placeholder' => 'Celular', 'class'=>'form-control telefone')) }}
		</div>
		<div class="col-xs-2">
			{{ Form::label('telefone', 'Telefone') }} <br>
			{{ Form::text('telefone', Input::old('telefone'), array('placeholder' => 'Telefone', 'class'=>'form-control telefone')) }}
		</div>
	</div>
		
	<div class="row">
	<br>
		<div class="col-xs-3 pull-right">
			{{ Form::submit('Criar nova conta', array('class'=>'btn btn-primary form-control')) }}	        
	    </div>
	</div>
	{{ Form::close() }}
	
@stop