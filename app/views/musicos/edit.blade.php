@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-11">
			<h3>Alterar Musico</h3>
		</div>
		<div class="col-md-1">
			{{ link_to('musicos', 'Voltar', array ('class' => 'btn btn-primary')) }}
		</div>
		<hr>
	</div>
	
	{{ Form::model ($musico, array ('url' => 'musicos/'.$musico->id, 'method'=>'put', 'class'=>'well', 'files' => true))  }}
	<div class="row">
		<div class="col-xs-9">
			<div class="row">
				<div class="col-xs-8">
					{{ Form::label('nome', 'Nome') }} <br>
					{{ Form::text('nome', Input::old('nome'), array('placeholder' => 'Nome', 'class'=>'form-control')) }}
				</div>
				<div class="col-xs-3">
					{{ Form::label('dt_nascimento', 'Data Nascimento') }} <br>
					{{ Form::text('dt_nascimento', Input::old('dt_nascimento'), array('placeholder' => 'Data Nascimento', 'class'=>'form-control data')) }}
				</div>
			</div>
			<div class="row">
				<br>
				<div class="col-xs-5">
					{{ Form::label('email', 'E-mail') }} <br>
					{{ Form::text('email', $musico->user->email, array('placeholder' => 'E-mail', 'class'=>'form-control', 'readonly'=>'readonly')) }}
				</div>
				<div class="col-xs-3">
					{{ Form::label('sexo', 'Sexo') }} <br>
					{{ Form::select('sexo', array('Feminino'=>'Feminino', 'Masculino'=>'Masculino'), $musico->sexo, array('class'=>'form-control')) }}
				</div>
				<div class="col-xs-3">
					{{ Form::label('instrumento_id', 'Instrumento') }} <br>
					{{ Form::select('instrumento_id', $instrumentos, $selected = NULL, array('class'=>'form-control')) }}
				</div>
				
			</div>
			<div class="row">
				<br>
				<div class="col-xs-3">
					{{ Form::label('cep', 'CEP') }} <br>
					{{ Form::text('cep', Input::old('cep'), array('placeholder' => 'CEP', 'class'=>'form-control cep')) }}
				</div>
				<div class="col-xs-2">
					{{ Form::label('numero', 'Numero') }} <br>
					{{ Form::text('numero', Input::old('numero'), array('placeholder' => 'N.', 'class'=>'form-control numero')) }}
				</div>
				<div class="col-xs-3">
					{{ Form::label('celular', 'Celular') }} <br>
					{{ Form::text('celular', Input::old('celular'), array('placeholder' => 'Celular', 'class'=>'form-control telefone')) }}
				</div>
				<div class="col-xs-3">
					{{ Form::label('telefone', 'Telefone') }} <br>
					{{ Form::text('telefone', Input::old('telefone'), array('placeholder' => 'Telefone', 'class'=>'form-control telefone')) }}
				</div>
			</div>
			<div class="row">
				<br><br>
				<div class="col-xs-7"></div>
				<div class="col-xs-4">
					{{ Form::submit('Salvar', array('class'=>'btn btn-primary form-control')) }}	        
			    </div>
			</div>
		</div>
		<div class="col-xs-3">
			<div class="row">
				<div class="col-xs-12">
					<div class="fileinput fileinput-new" data-provides="fileinput">
						<div class="fileinput-new thumbnail" >
						@if ($musico->imagem!=null)
			                <a href="<?php echo $musico->imagem; ?>">
			                	<img src="<?php echo $musico->imagem; ?>" alt=""> </a>
			            @else
			            	@if ($musico->sexo == "Masculino")
			                	<img src="/uploads/img/foto.jpg" alt="">
			                @else
								<img src="/uploads/img/foto2.jpg" alt="">
			                @endif
			            @endif
						</div>
						<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
						<div>
							<span class="btn btn-default btn-file"><span class="fileinput-new">Selecionar foto</span><span class="fileinput-exists">Mudar foto</span>{{ Form::file('fileImagem') }}</span>
							<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>

	
	{{ Form::close() }}
@stop