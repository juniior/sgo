@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-11">
			<h3>Apagar instrumento</h3>
		</div>
		<div class="col-md-1">
			{{ link_to('instrumentos', 'Voltar', array ('class' => 'btn btn-primary')) }}
		</div>
		<hr>
	</div>
	
	{{ Form::open ( array ('url' => 'instrumentos/'.$instrumento->id, 'method'=>'put', 'class'=>'well'))  }}
		<div class="row">
			<div class="col-xs-6">
				{{ Form::label('nome', 'Nome') }} <br>
				{{ Form::text('nome', Input::old('nome', $instrumento->nome), array('placeholder' => 'Nome', 'class'=>'form-control', 'readonly'=>'readonly')) }}
			</div>
			<div class="col-xs-3">
				{{ Form::label('tonalidade', 'Tonalidade') }} <br>
				{{ Form::select('tonalidade', array('B♭'=>'B♭', 'E♭'=>'E♭'), $instrumento->tonalidade, array('class'=>'form-control', 'readonly'=>'readonly')) }}
			</div>
			<div class="col-xs-12">
				{{ Form::label('descricao', 'Descrição') }} <br>
				{{ Form::textarea('descricao', Input::old('descricao', $instrumento->descricao), array('placeholder' => 'Descrição', 'class'=>'form-control', 'readonly'=>'readonly')) }}
			</div>
		</div>
		<div class="row">
		<br>
			<div class="col-xs-3 pull-right">
				{{ Form::open(array('url' => 'instrumentos/' . $instrumento->id, 'method' => 'delete', 'data-confirm' => 'Deseja realmente excluir o registro selecionado?')) }}
				{{ Form::button('Apagar Registro', array('type' => 'submit', 'class' => 'btn btn-danger form-control', 'title' => 'Apagar')) }}
				{{ Form::close() }}        
		    </div>
		</div>
	{{ Form::close() }}
@stop