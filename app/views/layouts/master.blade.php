<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>SGO - </title>
	{{ HTML::style('dist/css/main.css') }}
	{{ HTML::script('dist/js/jquery-2.1.0.min.js', array('defer' => 'defer')) }}
	{{ HTML::script('dist/js/bootstrap.min.js', array('defer' => 'defer')) }}
	{{ HTML::script('dist/js/jasny-bootstrap.min.js', array('defer' => 'defer')) }}
	{{ HTML::script('dist/js/chosen.jquery.js', array('defer' => 'defer')) }}
	{{ HTML::script('dist/js/jquery.maskedinput.min.js', array('defer' => 'defer')) }}
	{{ HTML::script('dist/js/main.js', array('defer' => 'defer')) }}
</head>
<body>	
	@include('partials._menu')
	
	<div class="container">
		@yield('content')
	</div>
	@include('partials._rodape')

</body>
</html>
