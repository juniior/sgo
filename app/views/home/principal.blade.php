@extends('layouts.master')

@section('content')

<div class="row">
	<div class="col-md-12">
		@if ( Session::get('error') )
		<div class="alert alert-error">{{{ Session::get('error') }}}</div>
		@endif

		@if ( Session::get('notice') )
		<div class="alert alert-info">{{{ Session::get('notice') }}}</div>
		@endif
	</div>

	@if (!Confide::user())
	<div class="col-md-7 mr20">
		<div class="row">
			<div class="col-md-12">
				<div class="row well well-sm">
					<h3>Seja bem-vindo a SGO</h3>
					<p>Sistema de Gerenciamento de Orquestra</p>
				</div>
				<div class="col-md-12">
					@if ( Session::get('error') )
					<div class="alert alert-error">{{{ Session::get('error') }}}</div>
					@endif

					@if ( Session::get('notice') )
					<div class="alert alert-info">{{{ Session::get('notice') }}}</div>
					@endif
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="row">
			<div class="col-md-12">
				<div class="row well well-sm">
					<form method="POST" class="form-signin" action="{{{ Confide::checkAction('UserController@do_login') ?: URL::to('/user/login') }}}" accept-charset="UTF-8">
						<input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
						<div class="form-group">
							<div class="col-md-12 mb10">
								<input class="form-control" tabindex="1" placeholder="{{{ Lang::get('confide::confide.username_e_mail') }}}" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">									
							</div>
							<div class="col-md-7">
								<input class="form-control" tabindex="2" placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="password">
							</div>
							<div class="col-md-5">
								<button tabindex="3" type="submit" class="btn btn-primary form-control">{{{ Lang::get('confide::confide.login.submit') }}}</button>
							</div>
						</div>

						<div class="col-md-12 form-group">
							<small>
								<label for="remember" class="checkbox">{{{ Lang::get('confide::confide.login.remember') }}}
									<input type="hidden" name="remember" value="0"> <input tabindex="4" type="checkbox" name="remember" id="remember" value="1">
									<a href="{{{ (Confide::checkAction('UserController@forgot_password')) ?: 'forgot' }}}">{{{ Lang::get('confide::confide.login.forgot_password') }}}</a>
								</label>
							</small>
						</div>
					</form>
				</div>

				{{ Form::open ( array ('url' => 'user', 'class'=>'row well well-sm'))  }}
					<div class="col-xs-12">
						<h4>É novo? Inscreva-se</h4>
					</div> 
					<div class="col-xs-12">
						{{ Form::text('username', Input::old('username'), array('placeholder' => 'Nome de usuário', 'class'=>'form-control mb10')) }}
						{{ Form::text('email', Input::old('email'), array('placeholder' => 'E-mail', 'class'=>'form-control mb10 ')) }}
						{{ Form::password('password', array('placeholder' => 'Senha', 'class'=>'form-control mb10')) }}
						{{ Form::password('password_confirmation', array('placeholder' => 'Confirmar senha', 'class'=>'form-control mb10')) }}
					</div>
					
					<div class="col-xs-8  pull-right">
						{{ Form::submit('Criar nova conta', array('class'=>'btn btn-default form-control')) }}	        
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
	@endif
</div>
	
@stop