@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-11">
			<h2><i class="glyphicon glyphicon-star"></i> Partituras</h2>
		</div>
		<div class="col-md-1">
			{{ link_to('partituras/create', 'Novo', array ('class' => 'btn btn-primary')) }}
		</div>
	</div>
	<div class="navbar-form navbar-right">
		{{ Form::open ( array ('url' => 'partituras', 'method' => 'get', 'class'=>'form-group ', 'role' => 'form'))  }}
			{{ Form::text('nome', $nome, array('placeholder' => 'Nome', 'class'=>'form-control')) }}
			{{ Form::button('Pesquisar', array('type' => 'submit', 'class' => 'btn  btn-success')) }}
		{{ Form::close() }}
		<hr>
	</div>
	<div class="row">
		@if($partituras->getItems())
			<table class="table table-bordered table-condensed table-striped">
				<thead>
					<tr>
						<th class="col-sm-2"><a href="{{ URL::to('intrumento?sort=nome'. $str) }}">Nome</a></th>
						<th class="col-sm-4">Instrumentos</th>
						<th class="col-sm-2">Observação</th>
						<th class="col-sm-1"></th>
					</tr>
				</thead>
				<tbody>
				@foreach ($partituras as $partitura)
					<tr>
						<td><span class="label label-info"> <i class="glyphicon glyphicon-star"></i> {{ e($partitura->autor) }}</span> {{ e($partitura->nome).' :: enviado por '. Musico::find($partitura->user->id)->nome }}</td>
						<td>
						@foreach($partitura->instrumentos as $instrumento)
							<span class="label label-warning"><i class="glyphicon glyphicon-star"></i> {{ $instrumento->nome}}</span>
						@endforeach
						</td>
						<td>{{ e($partitura->observacao) }}</td>
						<td>
							<div class="btn-group btn-group-xs">
{{ link_to('uploads/partituras/'.$partitura->arquivo, '', array('class' => 'btn btn-warning glyphicon glyphicon-download-alt', 'title'=>'Fazer download')) }}
{{ link_to('partituras/'.$partitura->id.'/edit', '', array('class' => 'btn btn-primary glyphicon glyphicon-edit', 'title'=>'Editar registro')) }}
{{ link_to('partituras/'.$partitura->id.'/apagar', '', array('class' => 'btn btn-danger glyphicon glyphicon-remove', 'title'=>'Apagar registro')) }}
							</div>
						</td>
						
					</tr>
				@endforeach
				</tbody>
			</table>
			<div id="data_paginate">
				{{ $pagination }}
				<p>Exibindo de {{ $partituras->getFrom() }} até {{ $partituras->getTo() }} de {{ $partituras->getTotal() }} registros.</p>
			</div>
		@else 
			<p class="label label-warning">Nenhum registro encontrado</p>
		@endif
	</div>
@stop