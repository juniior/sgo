@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-11">
			<h3>Adicionar Partitura</h3>
		</div>
		<div class="col-md-1">
			{{ link_to('partituras', 'Voltar', array ('class' => 'btn btn-primary')) }}
		</div>
		<hr>
	</div>
	
	{{ Form::open ( array ('url' => 'partituras', 'class'=>'well', 'files' => true))  }}
	<div class="row">
		<div class="col-xs-9">
			<div class="row">
				<div class="col-xs-7">
					{{ Form::label('nome', 'Nome') }} <br>
					{{ Form::text('nome', Input::old('nome'), array('placeholder' => 'Nome', 'class'=>'form-control')) }}
				</div>
				<div class="col-xs-5">
					{{ Form::label('autor', 'Autor') }} <br>
					{{ Form::text('autor', Input::old('autor'), array('placeholder' => 'Autor', 'class'=>'form-control')) }}
				</div>
			</div>
			<div class="row">
				<br>
				<div class="col-xs-12">
					{{ Form::label('filePartitura', 'Arquivo') }} <br>
					{{ Form::file('filePartitura', array('placeholder' => 'Arquivo da partitura ( PDF ou imagem )', 'class'=>'form-control')) }}
				</div>	
			</div>
			<div class="row">
				<br>
				<div class="col-xs-12">
					{{ Form::label('observacao', 'Observação') }} <br>
					{{ Form::textArea('observacao', Input::old('observacao'), array('placeholder' => 'Observação', 'class'=>'form-control')) }}
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
				{{ Form::label('instrumento_id[]', 'Instrumentos') }} <br>
				{{ Form::select('instrumento_id[]', $instrumentos, Input::old('instrumento_id'), array( 'multiple', 'style'=>'width:260px', 'class'=>'form-control chosen-select')) }}
			</div>
		</div>
	</div>
	
	
	<div class="row">
	<br>
		<div class="col-xs-3 pull-right">
			{{ Form::submit('Salvar', array('class'=>'btn btn-primary form-control')) }}	        
	    </div>
	</div>
	{{ Form::close() }}
@stop