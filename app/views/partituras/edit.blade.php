@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-11">
			<h3>Alterar partitura</h3>
		</div>
		<div class="col-md-1">
			{{ link_to('partituras', 'Voltar', array ('class' => 'btn btn-primary')) }}
		</div>
		<hr>
	</div>
	
	{{ Form::model ($partitura, array ('url' => 'partituras/'.$partitura->id, 'method'=>'put', 'class'=>'well')) }}
	<div class="row">
		<div class="col-xs-9">
			<div class="row">
				<div class="col-xs-7">
					{{ Form::label('nome', 'Nome') }} <br>
					{{ Form::text('nome', Input::old('nome'), array('placeholder' => 'Nome', 'class'=>'form-control')) }}
				</div>
				<div class="col-xs-5">
					{{ Form::label('autor', 'Autor') }} <br>
					{{ Form::text('autor', Input::old('autor'), array('placeholder' => 'Autor', 'class'=>'form-control')) }}
				</div>
			</div>	
			<div class="row">
				<br>
				<div class="col-xs-12">
					{{ Form::label('arquivo', 'Arquivo') }} <br>
					<div class="input-group">
						{{ Form::file('filePartitura', array('placeholder' => 'Arquivo da partitura ( PDF ou imagem )', 'class'=>'form-control')) }}
						 <span class="input-group-btn">
							{{ link_to('uploads/partituras/'.$partitura->arquivo, '', array('class' => 'btn btn-warning glyphicon glyphicon-download-alt', 'title'=>'Fazer download')) }}
      					</span>
					</div>	
				</div>	
			</div>
			<div class="row">
				<br>
				<div class="col-xs-12">
					{{ Form::label('observacao', 'Observação') }} <br>
					{{ Form::textArea('observacao', Input::old('observacao'), array('placeholder' => 'Observação', 'class'=>'form-control')) }}
				</div>
			</div>	
		</div>

		<div class="col-xs-3">
			<div class="row">
				<div class="col-xs-12">
				{{ Form::label('instrumento_id[]', 'Instrumentos') }} <br>
				{{ Form::select('instrumento_id[]', $instrumentos, $isps, array( 'multiple', 'style'=>'', 'class'=>'chosen-select form-control')) }}
				</div>
			</div>
		</div>
			
	</div>
	
	
	<div class="row">
		<br>
		<div class="col-xs-3 pull-right">
			{{ Form::submit('Salvar', array('class'=>'btn btn-primary form-control')) }}	        
	    </div>
	</div>
	{{ Form::close() }}
@stop