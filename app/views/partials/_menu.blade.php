<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			{{ link_to('principal', 'SGO', array('class'=>'navbar-brand')) }}
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				@if (Confide::user())
				<li>{{ link_to('instrumentos', 'Instrumentos') }}</li>
				<li>{{ link_to('musicos', 'Musicos') }}</li>
				<li>{{ link_to('partituras', 'Partituras') }}</li>
				<li>{{ link_to('categorias', 'Categorias') }}</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Olá, <b>{{ Musico::find(Confide::user()->id)->nome }}</b> <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>{{ link_to('musicos/'.Musico::find(Confide::user()->id)->id.'/edit', 'Perfil') }}</li>
						<li class="divider"></li>
						<li>{{ link_to('user/logout', 'Sair')}}</li>
					</ul>
				</li>
				@else
				<li>{{ link_to('user/create', 'Registrar-se') }}</li>
				<li>{{ link_to('user/login', 'Login') }}</li>
				@endif
			</ul>
		</div>
	</div>
</div>