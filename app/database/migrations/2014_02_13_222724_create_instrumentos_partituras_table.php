<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstrumentosPartiturasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('instrumentos_partituras', function(Blueprint $table)
		{
			//
			$table->increments('id');
			$table->integer('instrumento_id')->unsigned();
			$table->integer('partitura_id')->unsigned();

			$table->foreign('instrumento_id')->references('id')->on('instrumentos');
			$table->foreign('partitura_id')->references('id')->on('partituras');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('instrumentos_partituras');
	}

}