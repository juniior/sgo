<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMusicosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('musicos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nome', 100);
			$table->integer('user_id')->unsigned();
			$table->date('dt_nascimento');
			$table->text('sexo');
			$table->string('cep', 9);
			$table->string('numero', 5);
			$table->string('telefone', 14)->nullable();
			$table->string('celular', 14);
			$table->integer('instrumento_id')->unsigned();
			$table->string('imagem');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('instrumento_id')->references('id')->on('instrumentos');
			$table->foreign('user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('musicos');
	}

}
