<?php 
class InfoTableSeeder extends Seeder {
	public function run (){
		DB::table('instrumentos')->delete();

		Instrumento::create( array (
			'id'				=>	1,
			'nome'				=>	'Saxofone Tenor',
			'descricao'			=>	'Saxofone, também conhecido popularmente como sax, é um instrumento de sopro patenteado em 1846 pelo belga Adolphe Sax, um respeitado fabricante de instrumentos, que viveu na França no século XIX. Os saxofones são instrumentos transpositores, ou seja, a nota escrita não é a mesma nota que ouvimos (som real ou nota de efeito). A maior parte dos saxofones são em B♭ (como o sax tenor e o soprano) ou em E♭ (como o sax alto e o barítono).',
			'tonalidade'		=>	'B♭'
		));

		Instrumento::create( array (
			'id'				=>	2,
			'nome'				=>	'Saxofone Alto',
			'descricao'			=>	'Saxofone, também conhecido popularmente como sax, é um instrumento de sopro patenteado em 1846 pelo belga Adolphe Sax, um respeitado fabricante de instrumentos, que viveu na França no século XIX. Os saxofones são instrumentos transpositores, ou seja, a nota escrita não é a mesma nota que ouvimos (som real ou nota de efeito). A maior parte dos saxofones são em B♭ (como o sax tenor e o soprano) ou em E♭ (como o sax alto e o barítono).',
			'tonalidade'		=>	'E♭'
		));

		Instrumento::create( array (
			'id'				=>	3,
			'nome'				=>	'Trompete',
			'descricao'			=>	'O trompete ou trombeta é um instrumento musical de sopro, um aerofone da família dos metais (o trompete é o que produz o som mais agudo da família)1 , caracterizada por instrumentos de bocal, geralmente fabricados de metal. É também conhecido como pistão (pistom, por metonímia). Quem toca o trompete é chamado de trompetista.2
É constituído por corpo, chave de água, bomba de afinação, pistões, cotovelos e bocal, e terminado em pavilhão. É utilizado em diversos gêneros musicais, sendo muito comumente encontrado na música clássica, no jazz, bandas marciais e nos mariachis. Também é encontrado em estilos mais acelerados, como o frevo, o ska e latinos como o mambo e a salsa, bem como no maracatu rural, da zona da mata do norte de Pernambuco.',
			'tonalidade'		=>	'B♭'
		));

		Instrumento::create( array (
			'id'				=>	4,
			'nome'				=>	'Trombone B♭',
			'descricao'			=>	'',
			'tonalidade'		=>	'B♭'
		));

		Instrumento::create( array (
			'id'				=>	5,
			'nome'				=>	'Trombone E♭',
			'descricao'			=>	'',
			'tonalidade'		=>	'E♭'
		));


		Instrumento::create( array (
			'id'				=>	6,
			'nome'				=>	'Trompa B♭',
			'descricao'			=>	'',
			'tonalidade'		=>	'B♭'
		));


		Instrumento::create( array (
			'id'				=>	7,
			'nome'				=>	'Trompa Fá',
			'descricao'			=>	'',
			'tonalidade'		=>	'Fá'
		));


		Instrumento::create( array (
			'id'				=>	8,
			'nome'				=>	'Saxofone Barítono',
			'descricao'			=>	'',
			'tonalidade'		=>	'E♭'
		));

		Instrumento::create( array (
			'id'				=>	9,
			'nome'				=>	'Saxofone Soprano',
			'descricao'			=>	'',
			'tonalidade'		=>	'B♭'
		));

		Instrumento::create( array (
			'id'				=>	10,
			'nome'				=>	'Violino',
			'descricao'			=>	'',
			'tonalidade'		=>	'Dó'
		));

		Instrumento::create( array (
			'id'				=>	11,
			'nome'				=>	'Violoncelo',
			'descricao'			=>	'',
			'tonalidade'		=>	'Dó'
		));
		Instrumento::create( array (
			'id'				=>	12,
			'nome'				=>	'Clarinete',
			'descricao'			=>	'',
			'tonalidade'		=>	'B♭'
		));

		DB::table('users')->delete();

		$usuarios = array( 
			array (			
				'username'			=>	'admin',
				'email'				=>	'juniior.pvh@gmail.com',
				'password'			=>	Hash::make('123123'),
				'confirmation_code' => '483JU3ID8',
	            'confirmed' 		=> true
			),

			array (			
				'username'			=>	'regente',
				'email'				=>	'sidneymario@hotmail.com',
				'password'			=>	Hash::make('123456'),
				'confirmation_code' => '483JU3ID84545458181',
	            'confirmed' 		=> true
			),
		);

		$user = DB::table('users')->insert($usuarios);
		
		DB::table('musicos')->delete();

		Musico::create( array (
			'id'				=>	1,
			'nome'				=>	'Osvaldo dos Santos Junior',
			'user_id'			=>	1,
			'dt_nascimento'		=>	'1991-03-15',
			'sexo'				=>	'Masculino',
			'cep'				=>	'76807-778',
			'numero'			=>	'1672',
			'telefone'			=>	'(99) 9999-9999',
			'celular'			=>	'(99) 9999-9999',
			'instrumento_id'	=>	1
		));

		Musico::create( array (
			'id'				=>	2,
			'nome'				=>	'Mário Sidney',
			'user_id'			=>	2,
			'dt_nascimento'		=>	'1982-11-23',
			'sexo'				=>	'Masculino',
			'cep'				=>	'76807-778',
			'numero'			=>	'1672',
			'telefone'			=>	'(99) 9999-9999',
			'celular'			=>	'(99) 9999-9999',
			'instrumento_id'	=>	2
		));
	}
}