<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	//return Instrumento::first()->partituras;
	//return Partitura::first()->instrumentos;
	return View::make('home/principal');
});

Route::get('principal', 'HomeController@principal');

Route::resource('instrumentos', 'InstrumentosController');
Route::get('instrumentos/{id}/apagar', 'InstrumentosController@delete');

Route::resource('musicos', 'MusicosController');
Route::get('musicos/{id}/apagar', 'MusicosController@delete');

Route::resource('partituras', 'PartiturasController');
Route::get('partituras/{id}/apagar', 'PartiturasController@delete');

Route::resource('categorias', 'CategoriasController');
Route::get('categorias/{id}/apagar', 'CategoriasController@delete');

// Confide routes
Route::get( 'user/create', 					'UserController@create');
Route::post('user', 						'UserController@store');
Route::get( 'user/login', 					'UserController@login');
Route::post('user/login', 					'UserController@do_login');
Route::get( 'user/confirm/{code}', 			'UserController@confirm');
Route::get( 'user/forgot', 					'UserController@forgot_password');
Route::post('user/forgot_password', 		'UserController@do_forgot_password');
Route::get( 'user/reset_password/{token}', 	'UserController@reset_password');
Route::post('user/reset_password', 			'UserController@do_reset_password');
Route::get( 'user/logout', 					'UserController@logout');
