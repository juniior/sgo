<?php

class PartiturasController extends \BaseController {
	protected $partitura;

	public function __construct (Partitura $partitura, InstrumentoPartitura $instrumento_partitura){
		$this->partitura = $partitura;
		$this->instrumento_partitura = $instrumento_partitura;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$nome = null;

		$sort = 'nome';
		$order = Input::get('order') === 'desc' ? 'desc' : 'asc';

		$partituras = $this->partitura->orderBy($sort, $order);

		//dd($partituras->count());

		if (Input::has('nome')) {
			$partituras = $partituras->where('nome', 'LIKE', "%".Input::get('nome')."%");
			$nome = '&nome='.Input::get('nome');
		}

		$partituras = $partituras->paginate(15);

		$pagination = $partituras->appends( array (
			'nome' => Input::get('nome'),
			'sort' => Input::get('sort'),
			'order' => Input::get('order'),
		))->links();

		return View::make('partituras.index')->with( array (
			'nome' => Input::get('nome'),
			'partituras' => $partituras,
			'pagination' => $pagination,
			'str' => '&order='.(Input::get('order') == 'asc' || null ? 'desc' : 'asc').$nome
		));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$instrumentos = DB::table('instrumentos')->orderBy('nome', 'asc')->lists('nome','id');
		return View::make('partituras.create', array('instrumentos'=>$instrumentos));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$input = Input::all();

		$input['user_id'] = (int) Confide::user()->id;

		$partitura = $this->partitura->create($input);

		if (Input::hasFile('filePartitura')) {
		
			$file = Input::file('filePartitura');
 
			$destino = 'public/uploads/partituras/';
			$mime = $file->getClientOriginalExtension();
			$filename =  $partitura->nome . '-' . $partitura->id . '.' . $mime;
			
			$img = Input::file('filePartitura')->move($destino, $filename);
	
			if ($img) {
				$partitura->arquivo = $filename;
				$partitura->mime = $mime;
				$partitura->save();
			}
		}
		
		$instrumentos = Input::get('instrumento_id');

		foreach ($instrumentos as $instrumento_id) {

			$ip = [
				'instrumento_id'	=>	$instrumento_id,
				'partitura_id'		=>	$partitura->id
			];

			$this->instrumento_partitura->create($ip);
		}
			
		return Redirect::to('partituras');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$v['partitura'] = $this->partitura->find($id);
		
		$v['instrumentos'] = DB::table('instrumentos')->orderBy('nome', 'asc')->lists('nome','id');

		$isps = [];

		//return $v['partitura']->instrumentos;

		foreach ($v['partitura']->instrumentos as $inst)
			array_push($isps, $inst->id);

		$v['isps'] = $isps;

		if (is_null($v['partitura'])) {
			return Redirect::to('partituras');
		}

		return View::make('partituras.edit', $v);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$input = Input::all();		
		$input['id'] = $id;

		$partitura = $this->partitura->find($id);

		$this->instrumento_partitura->wherePartituraId($id)->delete();
		
		$instrumentos = Input::get('instrumento_id');

		foreach ($instrumentos as $instrumento_id) {

			$ip = [
				'instrumento_id'	=>	$instrumento_id,
				'partitura_id'		=>	$partitura->id
			];

			$this->instrumento_partitura->create($ip);
		}

		return Redirect::to('partituras');
	}

	public function delete($id){
		$v['partitura'] = $this->partitura->find($id);
		
		$v['instrumentos'] = DB::table('instrumentos')->orderBy('nome', 'asc')->lists('nome','id');

		if (is_null($v['partitura'])) {
			return Redirect::to('partituras');
		}

		return View::make('partituras.delete', $v);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		try {
			$this->partitura->find($id)->delete();
			return Redirect::to('partituras');
		} catch (Exception $e) {
			return Redirect::to('warning');
		}
	}

}