<?php

class CategoriasController extends \BaseController {
	protected $categoria;

	public function __construct (Categoria $categoria){
		$this->categoria = $categoria;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$nome = null;

		$sort = 'nome';
		$order = Input::get('order') === 'desc' ? 'desc' : 'asc';

		// Lista somentes as categorias pai (as que o campo categoria_id == null)
		$categorias = $this->categoria->whereNull('categoria_id')->orderBy($sort, $order);

		if (Input::has('nome')) {
			$categorias = $categorias->where('nome', 'LIKE', "%".Input::get('nome')."%");
			$nome = '&nome='.Input::get('nome');
		}

		$categorias = $categorias->paginate(15);

		$pagination = $categorias->appends( array (
			'nome' => Input::get('nome'),
			'sort' => Input::get('sort'),
			'order' => Input::get('order'),
		))->links();

		return View::make('categorias.index')->with( array (
			'nome' => Input::get('nome'),
			'categorias' => $categorias,
			'pagination' => $pagination,
			'str' => '&order='.(Input::get('order') == 'asc' || null ? 'desc' : 'asc').$nome
		));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//	
		$v['title']	 = "Cadastrar categoria";
		$v['categorias'] = array();

		if (Input::has('categoria_id')) {
			$v['title']	 = "Cadastrar sub-categoria";
			$v['categorias'] = $this->categoria->whereId(Input::get('categoria_id'))->lists('nome','id');
		} 
		return View::make('categorias.create', $v );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$input = Input::all();

		$cat = $this->categoria->create($input);

		if ($cat->categoria_id != null) {
			return Redirect::route('categorias.show', $cat->categoria_id);
		}
		return Redirect::to('categorias');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$v['categoria'] = $this->categoria->find($id);
		
		if (is_null($v['categoria'])) {
			return Redirect::to('categorias');
		}

		return View::make('categorias.show', $v);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$categoria = $this->categoria->find($id);

		if (is_null($categoria)) {
			return Redirect::to('categorias');
		}

		return View::make('categorias.edit')->with('categoria', $categoria);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$input = Input::all();
		$input['id'] = $id;

		$cat = $this->categoria->find($id);
		$cat->update($input);

		if ($cat->categoria_id != null) {
			return Redirect::route('categorias.show', $cat->categoria_id);
		}
		return Redirect::to('categorias');
	}

	public function delete($id){
		$categoria = $this->categoria->find($id);

		if (is_null($categoria)) {
			return Redirect::to('categorias');
		}

		return View::make('categorias.delete')->with('categoria', $categoria);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		try 
		{
			$cat = $this->categoria->find($id);
			$cat->delete();
			if ($cat->categoria_id != null) {
				return Redirect::route('categorias.show', $cat->categoria_id);
			}

			return Redirect::to('categorias');

		} 
		catch (Exception $e)
		{
			return Redirect::to('warning');
		}
	}

}