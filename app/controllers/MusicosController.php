<?php

class MusicosController extends \BaseController {
	protected $musico;

	public function __construct (Musico $musico){
		$this->musico = $musico;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$nome = null;

		$sort = 'nome';
		$order = Input::get('order') === 'desc' ? 'desc' : 'asc';

		$musicos = $this->musico->orderBy($sort, $order);

		if (Input::has('nome')) {
			$musicos = $musicos->where('nome', 'LIKE', "%".Input::get('nome')."%");
			$nome = '&nome='.Input::get('nome');
		}

		$musicos = $musicos->paginate(15);

		$pagination = $musicos->appends( array (
			'nome' => Input::get('nome'),
			'sort' => Input::get('sort'),
			'order' => Input::get('order'),
		))->links();

		return View::make('musicos.index')->with( array (
			'nome' => Input::get('nome'),
			'musicos' => $musicos,
			'pagination' => $pagination,
			'str' => '&order='.(Input::get('order') == 'asc' || null ? 'desc' : 'asc').$nome
		));
	}

	// /**
	//  * Show the form for creating a new resource.
	//  *
	//  * @return Response
	//  */
	// public function create()
	// {
	// 	//
	// 	$instrumentos = DB::table('instrumentos')->orderBy('nome', 'asc')->lists('nome','id');
	// 	return View::make('musicos.create', array('instrumentos'=>$instrumentos));
	// }

	// *
	//  * Store a newly created resource in storage.
	//  *
	//  * @return Response
	 
	// public function store()
	// {
	// 	//
	// 	$input = Input::all();
	// 	$input['dt_nascimento'] = Util::toMySQL($input['dt_nascimento']);
	// 	$input['senha'] = Hash::make($input['senha']);

	// 	$this->musico->create($input);
				
	// 	return Redirect::to('musicos');
	// }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$v['musico'] = $this->musico->find($id);
		$v['musico']->dt_nascimento = Util::toView($v['musico']->dt_nascimento);
		
		$v['instrumentos'] = DB::table('instrumentos')->orderBy('nome', 'asc')->lists('nome','id');

		if (is_null($v['musico'])) {
			return Redirect::to('musicos');
		}

		return View::make('musicos.edit', $v);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$input = Input::all();		
		$input['id'] = $id;
		
		$input['dt_nascimento'] = Util::toMySQL($input['dt_nascimento']);
	
		if (Input::hasFile('fileImagem')) {
			$img = Input::file('fileImagem');
			Image::make($img->getRealPath())->save('public/uploads/'.$id.'.png', 80);
			$input['imagem'] = '/uploads/'.$id.'.png';
		}

		$this->musico->find($id)->update($input);
		
		return Redirect::to('musicos');
	}

	public function delete($id){
		$musico = $this->musico->find($id);

		if (is_null($musico)) {
			return Redirect::to('musicos')->with('sucess', Util::message('MSG003'));
		}

		return View::make('musicos.delete')->with('musico', $musico);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		try {
			$this->musico->find($id)->delete();
			return Redirect::to('musicos');
		} catch (Exception $e) {
			return Redirect::to('warning');
		}
	}

}