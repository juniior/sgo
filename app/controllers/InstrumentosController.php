<?php

class InstrumentosController extends \BaseController {
	protected $instrumento;

	public function __construct (Instrumento $instrumento){
		$this->instrumento = $instrumento;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$nome = null;

		$sort = 'nome';
		$order = Input::get('order') === 'desc' ? 'desc' : 'asc';

		$instrumentos = $this->instrumento->orderBy($sort, $order);

		if (Input::has('nome')) {
			$instrumentos = $instrumentos->where('nome', 'LIKE', "%".Input::get('nome')."%");
			$nome = '&nome='.Input::get('nome');
		}

		$instrumentos = $instrumentos->paginate(15);

		$pagination = $instrumentos->appends( array (
			'nome' => Input::get('nome'),
			'sort' => Input::get('sort'),
			'order' => Input::get('order'),
		))->links();

		return View::make('instrumentos.index')->with( array (
			'nome' => Input::get('nome'),
			'instrumentos' => $instrumentos,
			'pagination' => $pagination,
			'str' => '&order='.(Input::get('order') == 'asc' || null ? 'desc' : 'asc').$nome
		));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View::make('instrumentos.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$input = Input::all();

		$this->instrumento->create($input);
		return Redirect::to('instrumentos');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$instrumento = $this->instrumento->find($id);

		if (is_null($instrumento)) {
			return Redirect::to('instrumentos');
		}

		return View::make('instrumentos.edit')->with('instrumento', $instrumento);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$input = Input::all();
		$input['id'] = $id;

		$this->instrumento->find($id)->update($input);
		return Redirect::to('instrumentos');
	}

	public function delete($id){
		$instrumento = $this->instrumento->find($id);

		if (is_null($instrumento)) {
			return Redirect::to('instrumentos');
		}

		return View::make('instrumentos.delete')->with('instrumento', $instrumento);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		try {
			$this->instrumento->find($id)->delete();
			return Redirect::to('instrumentos');
		} catch (Exception $e) {
			return Redirect::to('warning');
		}
	}

}