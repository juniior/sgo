$('.flash').click(function(e) {
	e.preventDefault();
	$(this).fadeOut('slow');
	return false;
});

$('.flash-success, .flash-info').animate({opacity: 1.0}, 3000).fadeOut('slow');

$('form[data-confirm]').submit(function() {
	var $tr = $(this).parent().parent();
	$tr.addClass('delete_entry');
    if(!confirm($(this).attr('data-confirm'))) {
    	$tr.removeClass('delete_entry');
        return false;
    }
});

$('.data').mask('99/99/9999', { placeholder: ' ' });
$('.telefone').mask('(99) 9999-9999', { placeholder: ' ' });
$('.cep').mask('99999-999', { placeholder: ' ' });

jQuery(document).ready(function($){
    var url = window.location.href;
    $('.nav a[href="'+url+'"]').parent().addClass('active');
});

$('.chosen-select').chosen({ no_results_text: 'Nenhum resultado encontrado.', max_selected_options: 20 });
$('.chosen-select-deselect').chosen({ allow_single_deselect: true, no_results_text: 'Nenhum resultado encontrado.' });